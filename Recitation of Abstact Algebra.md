# A First Course in Abstract Algebra with Applications

## Part.1

> H1.1
>
>> pass.
>>

> H1.2
>
> $\forall n\in \N \And n \ge 0$ $\forall r\in \R \And r \neq 1$
>
> 1. 证明等比数列求和公式$1+r+r^2+r^3+...+r^n = (1-r^{n+1})/(1-r)$
>
>    > p.f:  令 S = $1+r+r^2+r^3+...+r^n
>    >
> 2. 证明$1+2+2^2+2^3+...+2^n = 2^{n+1} - 1$
>
>    > pass.
>    >

> H1.3
>
>> pass.
>>
>
> H1.4
>
> 试证：若$0\le a\le b$，则对$\forall n \ge 0$，有$a^n \le b^n$
>
>> p.f:
>>
>> 1. $0= a\le b$时
>>
>> $\hspace{1.5em}a^n = 0, b^n \ge 0$，命题成立
>>
>> 2. $0\lt a \le b$时
>>    $\because n \ge 0, \therefore a^n \gt 0$
>>    $\frac {b^n} {a^n} = (\frac b a)^n \gt 1^n = 1$
>>    $a^n \le b^n$ 即证。
>>
>
> H1.5
>
> 试证：$1^2 + 2^2 + 3^2 + ... + n^2 = \frac 1 6 n(n+1)(2n+1)$
>
>> p.f.1:
>>
>> let $S_n = \sum a_n = n^3$
>>
>> thereforce $a_n = S_n - S_{n-1},S_0 = 0$
>>
>> $\hspace{9em}a_n =  n^3 - (n-1)^3 = 3n^2 - 3n + 1$
>>
>> p.f.2:
>>
>> $n^3 - (n-1)^3 = 3n^2 - 3n + 1$
>>
>> $(n-1)^3 - (n-2)^3 = 3(n-1)^2 - 3(n-1) + 1$
>>
>> ...
>>
>> add all, then
>>
>> $n^3 = 3\sum n^2 -3\sum n + n$
>>
>> $\sum n^2 = \frac 1 3 n^3 + \sum n - \frac 1 3 n$
>>
>> $\hspace{2.5em}=\frac 1 3 n^3 + \frac 1 2 n^2 + \frac 1 6 n$
>>
>> done.
>>
>> p.f.3:
>>
>> $n^2 = n(n-1) + n = 2\dbinom n 2 + \dbinom n 1$
>>
>> $\sum n^2 = 2\sum \dbinom n 2 + \sum \dbinom n 1$
>>
>> $\because \dbinom m k + \dbinom m k+1 = \dbinom m+1 k+1$
>>
>> $\therefore \sum n^2 = 2\dbinom {n+1} 3 + \dbinom {n+1} 2$
>>
>> $\hspace{3.5em}=\frac 1 3 n^3 + \frac 1 2 n^2 + \frac 1 6 n$
>>
>> done.
>>
>
> H1.6
>
> 试证：$1^3 + 2^3 + 3^3 + ... + n^3 = \frac 1 4 n^4 + \frac 1 2 n^3 + \frac 1 4 n^2$
>
>> p.f.1:
>>
>
> H1.12
>
> Alhazen formula: $(n+1)\displaystyle\sum_{i=1}^{n}i^k = \sum_{i=1}^{n}i^{k+1} + \sum_{i=1}^{n}\left(\sum_{p=1}^{i}p^k\right)$
>
