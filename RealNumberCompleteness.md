# 实数完备性

- [x] 戴德金原理 Dedekind Completeness.
  
  + **表述一：**
    
    > $\forall A \subset \R, \forall B \subset \R.$
    > 若
    > 
    > 1. $A \neq \text{\O}$ 且 $B \neq \text{\O}$
    > 2. $\forall a \in A, \forall b \in B$ 总有 $a \le b$
    > 
    > 则
    > $\exist c \in \R$, 使得$a\le{c}\le{b}$
  + **表述二：**
    
    > 若
    > 
    > 1. $A \subset \R, B \subset \R.$
    > 2. $A \neq \text{\O}$ 且 $B \neq \text{\O}$
    > 3. $A \bigcup B = \R$
    > 4. $\forall a \in A, \forall b \in B$ 总有 $a \le b$
    > 
    > 则
    > $\exist c \in \R$, 使得$a\le{c}\le{b}$

---

- [x] 确界存在原理 least-upper-bound property.
  
  > 对于$\forall S \subset \R \And S \neq \text{\O}$
  > 若
  > S 在$\R$内有上（下）界，
  > 则
  > S 在$\R$内有上（下）确界

---

- [x] 单调有界定理 monotone convergence theorem.
  
  > 若
  > 数列$A_n$单调递增(递减），且在$\R$内有上界（下界）
  > 则
  > 数列$A_n$收敛于$\R$内某个数

---

- [x] 闭区间套定理 nested intervals theorem.
  
  > 如果
  > 数列$\{a_n\},\{b_n\}$满足以下条件：
  > 
  > 1. 对$\forall n \in \Z^{+}$, $[a_n,b_n] \supe [a_n+1,b_n+1]$
  > 2. $\lim_{n\to\infty} (a_n - b_n) = 0$
  > 
  > 那么
  > $\exist! c \in \R$, 使得：
  > 
  > 1. $lim_{n\to\infty} (a_n) = lim_{n\to\infty} (b_n) = c$
  > 2. 对$\forall n \in \Z^{+}$,总有 $a_n \leq c \leq b_n$

---

- [x] 有限覆盖定理 Heine-Borel theorem.
  
  > 如果，
  > 由开区间组成的集合$E$能覆盖某个给定的闭区间$[a,b]$；
  > 那么，
  > $E$中可以找到有限个开区间，使得这有限个开区间也能覆盖$[a,b]$.
  > 
  > > 证明：

---

- [x] 聚点定理 Bolzano-Weierstrass theorem.
  
  > 对$\forall S \sub \R$,若$S$无穷且有界，则$S$至少有一个聚点.

---

- [x] 致密性定理 Bolzano-Weierstrass theorem.
  
  > 任意有界的数列都有收敛的子数列.

---

- [x] 柯西收敛原理 Cauchy completeness.
  
  > 数列$\{X_n\}$收敛的充分必要条件是
  > 
  > 对于$\forall \varepsilon \in \R^+;\exist N \in \Z^+ \; \forall m,n\gt N $ 使得 $|X_m - X_n| \lt \varepsilon$

---

- [x] 介值定理 intermediate value theorem.
  
  > 令$a,b,m \in \R$,
  > 若有函数$f:[a,b] \to \R$在$[a,b]$上连续，且$f(a) \lt m \lt f(b)$或$f(b) \lt m \lt f(a)$
  > 则$\exist c \in [a,b]$,使得$f(c) = m$.

---

- [x] 连通性定理 connectedness of reals.

---

